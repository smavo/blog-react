import React, { Component } from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';

import Header from './components/Header';
import Footer from './components/Footer';

import Home from './components/Home';
import Blog from './components/Blog';
import Error404 from './components/Error404';
import Search from './components/Search';
import Article from './components/Article';
import CreateArticle from './components/CreateArticle';
import EditArticle from './components/EditArticcle';
import ReactInfo from './components/ReactInfo';

class Router extends Component {
    render() {
        return (
            <BrowserRouter>

                <Header />

                {/* Configurando Rutas y  Paginas */}
                <Switch>

                    <Route exact path="/" activeClassName="active" component={Home} />
                    <Route exact path="/home" activeClassName="active" component={Home} />
                    <Route exact path="/blog" activeClassName="active" component={Blog} />
                    <Route exact path="/react" activeClassName="active" component={ReactInfo} />
                    <Route exact path="/blog/articulo/:id" component={Article} />
                    <Route exact path="/blog/busqueda/:search" component={Search} />
                    <Route exact path="/blog/crear" component={CreateArticle} />
                    <Route exact path="/blog/editar/:id" component={EditArticle} />
                    <Route exact path="/redirect/:search" render={
                        (props) => {
                            var search = props.match.params.search;
                            return (<Redirect to={'/blog/busqueda/' + search} />);
                        }
                    } />

                    <Route component={Error404} />

                </Switch>

                <div className="clearfix"></div>

                <Footer />

            </BrowserRouter>
        );
    }
}

export default Router; 