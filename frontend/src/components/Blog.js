import React, { Component } from 'react';
import Slider from './Slider';
import Sidebar from './Sidebar';
import Articles from './Articles';

class Blog extends Component {

    state = {
        articles: {},
        status: null
    }

    render() {

        return (
            <div id="blog">
                {/* <Slider   title="Blog Demo" /> */}
                <Slider title="Blog Demo" size="slider-small" />

                <div className="center">
                    <div id="content">
                        <br></br>
                        {/*<h1 className="subheader"> Blog React </h1>*/}
                    
                        {/* Listado de Articulos desde el API */}
                        <Articles/>

                    </div>

                    <Sidebar blog="true" />
                </div>
            </div>
        )
    }
}

export default Blog;