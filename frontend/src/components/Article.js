import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import axios from 'axios';
import Global from '../Global';
import Sidebar from './Sidebar';
import Moment from 'react-moment';
import 'moment/locale/es';
import imageDefault from '../assets/img/default.png';
import Swal from 'sweetalert2';

class Article extends Component {

    url = Global.url;

    state = {
        article: false,
        status: null
    }

    componentDidMount() {
        this.gerArticle();
    }

    gerArticle = () => {
        var id = this.props.match.params.id;

        axios.get(this.url + 'article/' + id)
            .then(res => {

                this.setState({
                    article: res.data.article,
                    status: 'success'
                });

            }).catch(err => {
                //console.log(err.response.data);
                this.setState({
                    article: false,
                    status: 'success'
                });
            });
    }

    deleteArticle = (id) => {

        Swal.fire({
            title: '¿Estas Seguro de Eliminar este Articulo?',
            text: "Si lo elimina, no podra recuperar el articulo eliminado",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Si, Eliminar!',
            cancelButtonText: 'No, Cancelar!',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {

                //alert(id);
                axios.delete(this.url + 'article/' + id)
                    .then(res => {
                        this.setState({
                            article: res.data.article,
                            status: 'deleted'
                        });
                        //console.log(this.state.article);
                    });

                Swal.fire(
                    'Articulo Eliminado!!',
                    'El articulo seleccionado ha sido eliminado del Blog',
                    'success'
                )
            } else if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.cancel
            ) {
                Swal.fire(
                    'Cancelado',
                    'El Articulo no se ha eliminado :)',
                    'error'
                )
            }
        });

    }


    render() {

        if (this.state.status === 'deleted') {
            return <Redirect to="/blog" />
        }

        var article = this.state.article;

        return (
            <div className="center">
                <section id="content">
                    <br></br>
                    {article &&
                        < article className="article-item article-detail">
                            <div className="image-wrap">
                                {article.image != null ? (
                                    <img src={this.url + "get-image/" + article.image} alt={article.title} />
                                ) : (
                                        <img src={imageDefault} alt={article.title} />
                                    )
                                }
                            </div>

                            <h1 className="subheader">{article.title}</h1>
                            <span className="date">
                                < Moment locale="es" fromNow date={article.date} />
                            </span>
                            <br></br>
                            <p> {article.content}</p>

                            <div className="buttons">
                                <button onClick={() => { this.deleteArticle(article._id); }}
                                    className="btn btn-danger ">Borrar</button>

                                <Link to={'/blog/editar/'+ article._id} className="btn btn-warning">Editar</Link>
                            </div>

                            <div className="clearfix"></div>
                        </article>

                    }

                    {!this.state.article && this.state.status === 'success' &&
                        <div className="alert alert-danger" role="alert">

                            <strong>El Articulo no existe. </strong> Intentelo mas tarde por favor
                    <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                    }

                    {this.state.status == null &&
                        <div className="alert alert-warning" role="alert">
                            <strong>Cargando.... </strong>  espere un momento por favor
                            <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                    }


                </section>

                <Sidebar />

            </div >
        )
    }
}

export default Article;