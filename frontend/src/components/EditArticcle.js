import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import axios from 'axios';
import Global from '../Global';
import Sidebar from './Sidebar';
import SimpleReactValidator from 'simple-react-validator';
import Swal from 'sweetalert2';
import imageDefault from '../assets/img/default.png';

class EditArticle extends Component {

    articleiD = null;
    url = Global.url;
    titleRef = React.createRef();
    contentRef = React.createRef();

    state = {
        article: {},
        status: null,
        selectedFile: null
    };

    componentDidMount() {
        this.articleiD = this.props.match.params.id;
        this.getArticle(this.articleiD);
    }

    constructor(props) {
        super(props);

        /*this.articleiD = this.props.match.params.id;
        this.getArticle(this.articleiD);*/

        this.validator = new SimpleReactValidator({
            messages: {
                required: 'Este campo es requerido'
            }
        });
    }

    getArticle = (id) => {
        axios.get(this.url + 'article/' + id)
            .then(res => {
                this.setState({
                    article: res.data.article
                })
            });
    }

    changeState = () => {
        this.setState({
            article: {
                title: this.titleRef.current.value,
                content: this.contentRef.current.value,
                image: this.state.article.image
            }
        });

        //console.log(this.state);
        this.validator.showMessages();
        this.forceUpdate();

    }


    saveArticle = (event) => {
        event.preventDefault();

        //Rellenar el state con la data del formulario
        this.changeState();

        if (this.validator.allValid()) {

            //Peticion Http por Post
            axios.put(this.url + 'article/' + this.articleiD, this.state.article)
                .then(res => {
                    if (res.data.article) {

                        this.setState({
                            article: res.data.article,
                            status: 'waiting'
                        });

                        Swal.fire(
                            'Articulo Actualizado!!',
                            'El Articulo se ha actualizado Correctamente',
                            'success'
                        );

                        //SUBIR IMAGE
                        if (this.state.selectedFile !== null) {
                            var articleId = this.state.article._id;

                            const formData = new FormData();

                            formData.append(
                                'file0',
                                this.state.selectedFile,
                                this.state.selectedFile.name
                            );

                            axios.post(this.url + 'upload-image/' + articleId, formData)
                                .then(res => {
                                    if (res.data.article) {
                                        this.setState({
                                            article: res.data.article,
                                            status: 'success'
                                        });
                                    } else {
                                        this.setState({
                                            article: res.data.article,
                                            status: 'failed'
                                        });
                                    }
                                });

                        } else {
                            this.setState({
                                status: 'success'
                            });
                        }

                    } else {

                        this.setState({
                            status: 'failed'
                        });

                        Swal.fire(
                            'Error al crear el Articulo',
                            'El Articulo no ha sido creado, intentelo nuevamente',
                            'error'
                        );

                    }
                });

        } else {
            this.setState({
                status: 'failed'
            });

            this.validator.showMessages();
            this.forceUpdate();
        }

    }

    fileChange = (event) => {
        event.preventDefault();
        //console.log(event);

        this.setState({
            selectedFile: event.target.files[0]
        });
        //console.log(this.state);

    }


    render() {
        //console.log(this.state.article);

        if (this.state.status === 'success') {
            return <Redirect to="/blog" />;
        }

        var article = this.state.article;

        return (
            <div className="center">
                <section id="content">
                    <br></br>
                    <h1 className="subheader">Editar Articulo</h1>

                    {this.state.article.title &&
                        <form className="mid-form" onSubmit={this.saveArticle}>

                            <div className="form-group">
                                <label htmlFor="title">Title</label>
                                <input type="text" name="title" defaultValue={article.title} ref={this.titleRef} onChange={this.changeState} />
                                {this.validator.message('title', this.state.article.title, 'required')}

                            </div>

                            <div className="form-group">
                                <label htmlFor="content">Contenido</label>
                                <textarea name="content" defaultValue={article.content} ref={this.contentRef} onChange={this.changeState}></textarea>
                                {this.validator.message('title', this.state.article.content, 'required')}

                            </div>

                            <div className="form-group">
                                <label htmlFor="file0">Imagen</label>

                                <input type="file" name="file0" onChange={this.fileChange} />
                                <div className="image-wrap">
                                    {article.image != null ? (
                                        <img src={this.url + "get-image/" + article.image} alt={article.title} className="thumb" />
                                    ) : (
                                            <img src={imageDefault} alt={article.title} className="thumb" />
                                        )
                                    }
                                </div>

                            </div>
                            <div className="clearfix"></div>
                            <br></br> 
                            <input type="submit" value="Guardar" className="btn btn-success" />
                            <br></br>
                        </form>
                        
                    }

                    {!this.state.article.title &&
                        <div className="alert alert-warning" role="alert">
                            <strong>Cargando.... </strong>  espere un momento por favor
                            <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    }

                </section>
                <Sidebar />
            </div>
        );
    }
}

export default EditArticle;