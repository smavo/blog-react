import React, { Component } from 'react';
import { FaInstagram } from "react-icons/fa";
import { FaTwitter } from "react-icons/fa";
import { FaFacebookF } from "react-icons/fa";
import { FaLinkedin } from "react-icons/fa";
import { FaGitlab } from "react-icons/fa";

class Footer extends Component {
    render() {
        return (
            <footer id="footer" className="bg-dark">
                <div className="center">
                    <nav className="redes-sociales">
                        <a href="https://gitlab.com/smavo"><i> <FaGitlab/> </i></a>
                        <a href="https://www.linkedin.com/in/smavo24/"><i> <FaLinkedin/> </i></a>
                        <a href="https://twitter.com/smavo24"><i> <FaTwitter/> </i></a>
                        <a href="https://www.instagram.com/s.m.a.v.o.24/"> <i> <FaInstagram/></i> </a>
                        <a href="https://www.facebook.com/smavo24"><i> <FaFacebookF/> </i></a> 
                    </nav>
                </div>
            </footer>
        );
    }
}

export default Footer;