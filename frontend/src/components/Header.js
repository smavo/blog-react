import React, { Component } from 'react';
import logo from '../assets/img/logo.png'
import { NavLink } from 'react-router-dom';

class Header extends Component {

    render() {
        return (
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark">

                <a className="navbar-brand" href="/home">
                    <img src={ logo } width="30" height="30" className="d-inline-block align-top" alt="" loading="lazy"></img>
                    <span> Blog</span> React </a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>

                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item ">
                            <NavLink to="/home" className="nav-link">Inicio</NavLink>
                        </li>

                        <li className="nav-item">
                            <NavLink to="/blog" className="nav-link">Blog</NavLink>
                        </li>

                        <li className="nav-item">
                            <NavLink to="/react" className="nav-link">React</NavLink>
                        </li>

                    </ul>
                </div>
            </nav>
        );
    }
}

export default Header;