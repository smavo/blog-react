import React, { Component } from 'react';
import Slider from './Slider';
import Sidebar from './Sidebar';
import Articles from './Articles';

class Search extends Component {

    render() {

        var searched = this.props.match.params.search;

        return (
            <div id="blog">
                {/* <Slider   title="Blog Demo" /> */}
                <Slider title={'Busqueda: '+ searched} size="slider-small" />

                <div className="center">
                    
                    <div id="content">
                    <br></br>
                        <h1 className="subheader"> Ultimos Articulos</h1>
                        {/*<h1 className="subheader"> Blog React </h1>*/}
                        {/* Listado de Articulos desde el API */}
                        
                        <Articles 
                            search={searched}
                        />

                    </div>

                    <Sidebar blog="true" />

                </div>
            </div>
        )
    }
}

export default Search;