import React, { Component } from 'react';

class Error404 extends Component {
    render() {
        return (

            <div className="center">
                <br></br>
                <div className="alert alert-success" role="alert">
                    <h4 className="alert-heading">Pagina no Encontrada</h4>
                    <p>No se encontro lo solicitado</p>
                    <br></br>
                    <p className="mb-0">Intentelo mas Tarde</p>
                </div>

            </div>
        );
    }
}

export default Error404;
