import React, { Component } from "react";
import logo from "../../src/logo.svg";
import Sidebar from "./Sidebar";

class ReactInfo extends Component {
  render() {
    return (
      <div id="reactinfo" className="App-header-0">
        
        <div className="center">
        <div id="content">
          <br></br>
          <h2 className="subheader App-link-0">REACT 16.13.1 </h2>
          <img src={logo} className="App-logo-0" alt="logo" />
          <p className="App-link-0">
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <a
            className="App-link-0"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
        </div>

        <Sidebar  blog="true"/>
        </div>
        </div>

    );
  }
}

export default ReactInfo;
