import React from 'react';
import './App.css';
import './components/Header';

import Router from './Router';

function App() {
  return (
    <div className="App">

        <section className="componentes">
          <Router />
        </section>
        
    </div>
  );
}

export default App;
